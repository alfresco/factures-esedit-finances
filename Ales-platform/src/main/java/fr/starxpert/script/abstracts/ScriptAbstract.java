package fr.starxpert.script.abstracts;

public abstract class ScriptAbstract {
	
	public abstract void execute(Object[] args);

	public abstract void injectionServices(Object[] args);

}