package fr.starxpert.script;

import java.util.List;
import java.util.Map;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.security.AuthorityService;
import org.alfresco.service.cmr.security.PermissionService;
import org.alfresco.service.cmr.site.SiteInfo;
import org.alfresco.service.cmr.site.SiteService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.starxpert.bean.Correspondance;
import fr.starxpert.bean.Document;
import fr.starxpert.bean.Groupe;
import fr.starxpert.jobs.ServiceCommon;
import fr.starxpert.model.AlesDocumentModel;
import fr.starxpert.script.abstracts.ScriptAbstract;
import fr.starxpert.service.GroupeService;

public class ScriptGestionDroits extends ScriptAbstract{
	
	private static Log logger = LogFactory.getLog(ScriptGestionDroits.class);
	
	private ServiceRegistry registryService;
	
	@Autowired
	private NodeService nodeService;
	
	@Autowired
	private AuthorityService authorityService;
	
	private GroupeService groupeService;
	
	private ServiceCommon serviceCommon;
	
	private SiteService siteService;
	
	@Autowired
	private PermissionService permissionService;
	
	private final String PREFIX_GROUP = "GROUP_";
		
	public ScriptGestionDroits() { }
	
	public void createGroupsFromServices(Correspondance correspondance){
		
		// Recuperer le plan de classement
		
		logger.debug("Service common : " + this.serviceCommon);
		
		logger.debug("Service groupe : " + this.groupeService);
		
		// Map< CodeService, Map<IndexColonne, Label>>
		
		logger.debug("Correspondance file Name : " + correspondance.getNomFichier());
		Map<String, Map<String, String>> correspondanceService = serviceCommon.getCorrespondance(correspondance.getNomFichier());
	
		this.groupeService.createGroupList("Services", correspondanceService, correspondance.getNumeroColonne());
		
	}
	
	
	public void createUsersOfGroups(Correspondance correspondance) {
		
		Map<String, Map<String, String>> correspondanceUsers = serviceCommon.getCorrespondance(correspondance.getNomFichier());
		
		this.groupeService.createUsersofGroups(correspondanceUsers, correspondance.getNumeroColonne());
		
		
	}
	
	
	/** Methode appelee pour chaque classement
	 * 
	 */
	@Override
	public void execute(Object[] args) {
		// TODO Auto-generated method stub
		
		// retirer l'héritage pour chaque dossier
		
		logger.debug("************Execution ScriptGestionDroits**********************");
		
		// On cast les variables qui sont envoyés a cette methode
		NodeRef nodeRefDestination = (NodeRef) args[0];
		Document document = (Document) args[1];
		List<Correspondance> listeCorrespondance = (List<Correspondance>) args[2];
		
		

		// Nom de la metadonnée a utiliser pour la creation des groupes
		String metadonneePourCreationGroupe = "als:codeService";
		
		Correspondance correspondanceSelected = null;
		
		// Recuperation de la table de correspondance associée à la metadonnée "metadonneePourCreationGroupe"
		for (Correspondance correspondance : listeCorrespondance) {
			
			if(metadonneePourCreationGroupe.equals(correspondance.getNommetadonnee())){
			
				correspondanceSelected = correspondance;
			}
		}
		
		if(correspondanceSelected != null) {
			
		// ON cree les groupes a partir de la table de correspondance codeService si ils n'existent pas	
		createGroupsFromServices(correspondanceSelected);
		
		
		
			
		// On recupere le service dans lequel le document va etre classé
		String serviceName = (String) document.getMetadonneesDocument().get(AlesDocumentModel.QNAME_ASPECT_CODE_SERVICE);
		
		logger.debug("**********serviceName*******: "+serviceName);
		
		// Si le service existe alors : 
		if(serviceName != null) {
				
				String siteName = document.getNomSite();
				
				// On recupere le document library du site
				
				SiteInfo siteInfos =  this.registryService.getSiteService().getSite(siteName);
				NodeRef documentLibraryNodeRef =  this.registryService.getNodeService().getChildByName(siteInfos.getNodeRef(), ContentModel.ASSOC_CONTAINS, "DocumentLibrary");
				
				logger.debug("documentLibraryNodeRef : " + documentLibraryNodeRef);
				
				String groupName = PREFIX_GROUP + serviceName;
				
				logger.debug("groupName : " + groupName);
				
				logger.debug("groupeService.getListeGroupes() : " + groupeService.getListeGroupes());
				
				Groupe groupSelected = groupeService.getListeGroupes().get(serviceName);
				
				logger.debug("groupSelected : " + groupSelected.getAuthorityName());
				
				//La métadonnée <code service> est existante
				if(groupSelected != null) {
										 					
					 AuthenticationUtil.runAsSystem(() -> { 
						 
						 String authority = groupSelected.getAuthorityName();
						 
						 NodeRef nodeRefFolder = nodeRefDestination;
						 
						 String permissions = PermissionService.CONTRIBUTOR;
						 
						 // On remonte dans l'arborescence et on donne les droits a chaque dossier parent jusqu'au document
						 // library
						 while(true) {
						 
						  this.registryService.getPermissionService().setPermission(nodeRefFolder, authority, permissions, true);
							  						 
							  NodeRef parentNodeRef = this.registryService.getNodeService().getPrimaryParent(nodeRefFolder).getParentRef();
							  
							  logger.debug(this.registryService.getNodeService().getProperty(parentNodeRef, ContentModel.PROP_NAME));
							  
							  
							  logger.debug("is documentLibrary : " + parentNodeRef.toString().equals(documentLibraryNodeRef.toString()));
							  
							  if(!parentNodeRef.toString().equals(documentLibraryNodeRef.toString())) {
								  
								  nodeRefFolder = parentNodeRef;
								  
								  // On met le droit a consumer pour les autres dossiers afin de permettre aux membres
								  // du service d'acceder a leur dossier
								  permissions = PermissionService.CONSUMER;
							
							  // On s'arrete au documentLibrary du site	  
							  } else {
								  
								  this.registryService.getPermissionService().setPermission(nodeRefFolder, authority, permissions, true);
								  break;
								  
							  }
						}
							
						 return null;    
			           
					});
					
				};
				
				// recuperer le groupe qui convient : 
				// String groupeService = this.listeGroupes.get(serviceName);
			
				// Ajout du groupe au site
				
				// Suppression des droits
				
				// On efface toutes le permissions et l'héritage
				// permissionService.clearPermission(folderNodeRef, null);
				// permissionService.deletePermissions(folderNodeRef);
				// permissionService.setInheritParentPermissions(folderNodeRef, false);
				
				// appliquer les droits lies au service pour chaque dossiers
				
				// appliiquer les droits pour le super admin 
			}
		}
		
		
		//parite 2 : création des utilisateurs pour chaque groupe 
				String metadonneePourCreationUsers = "als:alesUsers";
				
				Correspondance correspondanceToUse = null;
				
				// Recuperation de la table de correspondance associée à la metadonnée "metadonneePourCreationUsers"
				for (Correspondance correspondance : listeCorrespondance) {
					
					if(metadonneePourCreationUsers.equals(correspondance.getNommetadonnee())){
					
						correspondanceToUse = correspondance;
					}
				}
		
		
		//création des utilisateurs associé à chaque groupe
				createUsersOfGroups(correspondanceToUse);
				
		
	}
	
	
	@Override
	public void injectionServices(Object[] args) {
		
		this.groupeService = (GroupeService) args[0];
		this.serviceCommon =  (ServiceCommon) args[1];
		this.registryService = (ServiceRegistry) args[2];

		
	}
	
	

}
