package fr.starxpert.model;

import org.alfresco.service.cmr.repository.NodeRef;

public class InformationsClassement {
	
	private NodeRef parentNodeRef;
	
	private String cheminFichierCSV;
	
	private int nombreFichersErreurs;
	
	private String NomFichierCsv;
	
	/**
	 * @return the parentNodeRef
	 */
	public NodeRef getParentNodeRef() {
		return parentNodeRef;
	}
	/**
	 * @param parentNodeRef the parentNodeRef to set
	 */
	public void setParentNodeRef(NodeRef parentNodeRef) {
		this.parentNodeRef = parentNodeRef;
	}
	/**
	 * @return the cheminFichierCSV
	 */
	public String getCheminFichierCSV() {
		return cheminFichierCSV;
	}
	/**
	 * @param cheminFichierCSV the cheminFichierCSV to set
	 */
	public void setCheminFichierCSV(String cheminFichierCSV) {
		this.cheminFichierCSV = cheminFichierCSV;
	}
	/**
	 * @return the nombreFichersErreurs
	 */
	public int getNombreFichersErreurs() {
		return nombreFichersErreurs;
	}
	/**
	 * @param nombreFichersErreurs the nombreFichersErreurs to set
	 */
	public void setNombreFichersErreurs(int nombreFichersErreurs) {
		this.nombreFichersErreurs = nombreFichersErreurs;
	}
	/**
	 * @return the nomFichierCsv
	 */
	public String getNomFichierCsv() {
		return NomFichierCsv;
	}
	/**
	 * @param nomFichierCsv the nomFichierCsv to set
	 */
	public void setNomFichierCsv(String nomFichierCsv) {
		NomFichierCsv = nomFichierCsv;
	}
	
	


}
