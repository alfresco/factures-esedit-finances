package fr.starxpert.model;

import org.alfresco.service.cmr.repository.NodeRef;

public class InfosMails {
	
	
	private String periodeLancement;
    
    private String cheminCsv;
    
    private String emailDestination;
    
    private NodeRef nodeReeDossierCsv;
    
    private boolean isError;
    
    public InfosMails() {
		// TODO Auto-generated constructor stub
	}
    
	public InfosMails(String periodeLancement, String cheminCsv, String emailDestination,  NodeRef nodeReeDossierCsv) {
		this.periodeLancement = periodeLancement;
		this.cheminCsv = cheminCsv;
		this.emailDestination = emailDestination;
		this.nodeReeDossierCsv = nodeReeDossierCsv;
	}



	public String getPeriodeLancement() {
		return periodeLancement;
	}

	public void setPeriodeLancement(String periodeLancement) {
		this.periodeLancement = periodeLancement;
	}

	public String getCheminCsv() {
		return cheminCsv;
	}

	public void setCheminCsv(String cheminCsv) {
		this.cheminCsv = cheminCsv;
	}

	public String getEmailDestination() {
		return emailDestination;
	}

	public void setEmailDestination(String emailDestination) {
		this.emailDestination = emailDestination;
	}

	public NodeRef getNodeReeDossierCsv() {
		return nodeReeDossierCsv;
	}

	public void setNodeReeDossierCsv(NodeRef nodeReeDossierCsv) {
		this.nodeReeDossierCsv = nodeReeDossierCsv;
	}

	public boolean isError() {
		return isError;
	}

	public void setError(boolean isError) {
		this.isError = isError;
	}
    
    
    

}
