package fr.starxpert.bean;

import java.util.List;

/** Un bean permettant de recuperer un ensemble non cohérent de
 * valeurs issues d'une methode. a completer un fur et a mesure des besoins. 
 * 
 * @author omardelaa
 *
 */
public class ReturnGenericObject {
	
	private Classement classement;
	
	private List<Correspondance> listeCorrespondance;
	
	public ReturnGenericObject() {
		// TODO Auto-generated constructor stub
	}

	public Classement getClassement() {
		return classement;
	}

	public void setClassement(Classement classement) {
		this.classement = classement;
	}

	public List<Correspondance> getListeCorrespondance() {
		return listeCorrespondance;
	}

	public void setListeCorrespondance(List<Correspondance> listeCorrespondance) {
		this.listeCorrespondance = listeCorrespondance;
	}
	
	
	
	

}
