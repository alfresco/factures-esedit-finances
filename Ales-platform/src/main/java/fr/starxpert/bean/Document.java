package fr.starxpert.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.alfresco.service.cmr.repository.NodeRef;

import org.alfresco.service.namespace.QName;

public class Document {
	
	
	
	private Map<QName, Serializable>  metadonneesDocument;
	
	private NodeRef nodeRef;
	
	private String nomDocument;
	
	private String nomSite;

	private String pathDoc;
	
	private String metadonnesManquante;
	
	//liste métadonne à récuperer pour la création de l'arborescence
    private List<String> metadataDoc ;
	

	
	
	
	public Map<QName, Serializable> getMetadonneesDocument() {
		return metadonneesDocument;
	}

	public void setMetadonneesDocument(Map<QName, Serializable> metadonneesDocument) {
		this.metadonneesDocument = metadonneesDocument;
	}

	/**
	 * @return the nodeRef
	 */
	public NodeRef getNodeRef() {
		return nodeRef;
	}

	/**
	 * @param nodeRef the nodeRef to set
	 */
	public void setNodeRef(NodeRef nodeRef) {
		this.nodeRef = nodeRef;
	}

	/**
	 * @return the nomDocument
	 */
	public String getNomDocument() {
		return nomDocument;
	}

	/**
	 * @param nomDocument the nomDocument to set
	 */
	public void setNomDocument(String nomDocument) {
		this.nomDocument = nomDocument;
	}

	/**
	 * @return the nomSite
	 */
	public String getNomSite() {
		return nomSite;
	}

	/**
	 * @param nomSite the nomSite to set
	 */
	public void setNomSite(String nomSite) {
		this.nomSite = nomSite;
	}

	/**
	 * @return the pathDoc
	 */
	public String getPathDoc() {
		return pathDoc;
	}

	/**
	 * @param pathDoc the pathDoc to set
	 */
	public void setPathDoc(String pathDoc) {
		this.pathDoc = pathDoc;
	}

	/**
	 * @return the metadonnesManquante
	 */
	public String getMetadonnesManquante() {
		return metadonnesManquante;
	}

	/**
	 * @param metadonnesManquante the metadonnesManquante to set
	 */
	public void setMetadonnesManquante(String metadonnesManquante) {
		this.metadonnesManquante = metadonnesManquante;
	}

	/**
	 * @return the metadataDoc
	 */
	public List<String> getMetadataDoc() {
		return metadataDoc;
	}

	/**
	 * @param metadataDoc the metadataDoc to set
	 */
	public void setMetadataDoc(List<String> metadataDoc) {
		this.metadataDoc = metadataDoc;
	}

	


	
	
	
	
	
	

}
