package fr.starxpert.bean;

import java.util.Map;

public class Correspondance {

	
	private String nommetadonnee;
	
	private String nomFichier;
	
	private String numeroColonne;
	
	private Map<String, Map<String, String>> tableCorrespondance;
	
	/**
	 * @return the nommetadonnee
	 */
	public String getNommetadonnee() {
		return nommetadonnee;
	}
	/**
	 * @param nommetadonnee the nommetadonnee to set
	 */
	public void setNommetadonnee(String nommetadonnee) {
		this.nommetadonnee = nommetadonnee;
	}
	/**
	 * @return the nomFichier
	 */
	public String getNomFichier() {
		return nomFichier;
	}
	/**
	 * @param nomFichier the nomFichier to set
	 */
	public void setNomFichier(String nomFichier) {
		this.nomFichier = nomFichier;
	}
	/**
	 * @return the numeroColonne
	 */
	public String getNumeroColonne() {
		return numeroColonne;
	}
	/**
	 * @param numeroColonne the numeroColonne to set
	 */
	public void setNumeroColonne(String numeroColonne) {
		this.numeroColonne = numeroColonne;
	}
	/**
	 * @return the tableCorrespondance
	 */
	public Map<String, Map<String, String>> getTableCorrespondance() {
		return tableCorrespondance;
	}
	/**
	 * @param tableCorrespondance the tableCorrespondance to set
	 */
	public void setTableCorrespondance(Map<String, Map<String, String>> tableCorrespondance) {
		this.tableCorrespondance = tableCorrespondance;
	}
    
    
    
}
