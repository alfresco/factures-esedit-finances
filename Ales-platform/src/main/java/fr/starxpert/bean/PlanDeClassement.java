package fr.starxpert.bean;

import java.util.List;

public class PlanDeClassement {
	
	private List<Correspondance> correspondances;
    private List<Classement> classement;
	/**
	 * @return the correspondances
	 */
	public List<Correspondance> getCorrespondances() {
		return correspondances;
	}
	/**
	 * @param correspondances the correspondances to set
	 */
	public void setCorrespondances(List<Correspondance> correspondances) {
		this.correspondances = correspondances;
	}
	/**
	 * @return the classement
	 */
	public List<Classement> getClassement() {
		return classement;
	}
	/**
	 * @param classement the classement to set
	 */
	public void setClassement(List<Classement> classement) {
		this.classement = classement;
	}
    
    
    

}
