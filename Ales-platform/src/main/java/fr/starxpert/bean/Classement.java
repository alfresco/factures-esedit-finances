package fr.starxpert.bean;

public class Classement {

	 private String dossierDestination;
	    
	 private String site;
	    
	 private Condition condition;
	    
	 private String dossierErreur;
	    
	 private String script;

	/**
	 * @return the dossierDestination
	 */
	public String getDossierDestination() {
		return dossierDestination;
	}

	/**
	 * @param dossierDestination the dossierDestination to set
	 */
	public void setDossierDestination(String dossierDestination) {
		this.dossierDestination = dossierDestination;
	}

	/**
	 * @return the site
	 */
	public String getSite() {
		return site;
	}

	/**
	 * @param site the site to set
	 */
	public void setSite(String site) {
		this.site = site;
	}

	/**
	 * @return the condition
	 */
	public Condition getCondition() {
		return condition;
	}

	/**
	 * @param condition the condition to set
	 */
	public void setCondition(Condition condition) {
		this.condition = condition;
	}

	/**
	 * @return the dossierErreur
	 */
	public String getDossierErreur() {
		return dossierErreur;
	}

	/**
	 * @param dossierErreur the dossierErreur to set
	 */
	public void setDossierErreur(String dossierErreur) {
		this.dossierErreur = dossierErreur;
	}

	/**
	 * @return the script
	 */
	public String getScript() {
		return script;
	}

	/**
	 * @param script the script to set
	 */
	public void setScript(String script) {
		this.script = script;
	}
	 
	 
	 
}
