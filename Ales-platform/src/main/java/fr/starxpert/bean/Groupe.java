package fr.starxpert.bean;

import org.alfresco.service.cmr.repository.NodeRef;

public class Groupe {
	
	private NodeRef nodeRef;
	private String authorityName;
	private String label;
	
	public Groupe(NodeRef nodeRef, String authorityName, String label) {
		this.nodeRef = nodeRef;
		this.label = label;
		this.authorityName = authorityName;
	}

	public NodeRef getNodeRef() {
		return nodeRef;
	}

	public void setNodeRef(NodeRef nodeRef) {
		this.nodeRef = nodeRef;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getAuthorityName() {
		return authorityName;
	}

	public void setAuthorityName(String authorityName) {
		this.authorityName = authorityName;
	}
	
	

	

}
