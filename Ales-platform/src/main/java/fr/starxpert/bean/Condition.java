package fr.starxpert.bean;


import java.util.List;
import java.util.Map;

public class Condition {
	
	private Map<String, List<String>> metadata;
	
	
    private List<String> types;
    
    private List<String> aspects;
	
	
    
    
    
	/**
	 * @return the metadata
	 */
	public Map<String, List<String>> getMetadata() {
		return metadata;
	}
	/**
	 * @param metadata the metadata to set
	 */
	public void setMetadata(Map<String, List<String>> metadata) {
		this.metadata = metadata;
	}
	/**
	 * @return the types
	 */
	public List<String> getTypes() {
		return types;
	}
	/**
	 * @param types the types to set
	 */
	public void setTypes(List<String> types) {
		this.types = types;
	}
	/**
	 * @return the aspects
	 */
	public List<String> getAspects() {
		return aspects;
	}
	/**
	 * @param aspects the aspects to set
	 */
	public void setAspects(List<String> aspects) {
		this.aspects = aspects;
	}
    
    
    

}
