package fr.starxpert.workers;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.alfresco.repo.batch.BatchProcessor.BatchProcessWorkerAdaptor;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.springframework.beans.factory.annotation.Autowired;


import fr.starxpert.classement.ClassementDocument;


public class ClassementWorker extends BatchProcessWorkerAdaptor<NodeRef> {

	private static Logger LOGGER = Logger.getLogger(ClassementWorker.class.getName());
	
	@Autowired
	private NodeService nodeService;
	
	
	private ClassementDocument  classementDocument;
	
	
	/**
	 * @return the classementDocument
	 */
	public ClassementDocument getClassementDocument() {
		return classementDocument;
	}

	/**
	 * @param classementDocument the classementDocument to set
	 */
	public void setClassementDocument(ClassementDocument classementDocument) {
		this.classementDocument = classementDocument;
	}

	public ClassementWorker(ClassementDocument classementDocument) {
		this.classementDocument = classementDocument;
	}
	
	@Override
	public void process(NodeRef nodeRef) throws Throwable {
		
		classementDocument.classementGenerique(nodeRef);
	
	}

	
	/**
	 * @return the nodeService
	 */
	public NodeService getNodeService() {
		return nodeService;
	}

	/**
	 * @param nodeService the nodeService to set
	 */
	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}
	
	
	

}
