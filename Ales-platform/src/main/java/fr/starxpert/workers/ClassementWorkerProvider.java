package fr.starxpert.workers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.alfresco.repo.batch.BatchProcessWorkProvider;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.util.VmShutdownListener;


public class ClassementWorkerProvider implements BatchProcessWorkProvider<NodeRef> {
	
	
	private final VmShutdownListener vmShutdownListener = new VmShutdownListener("getClassementWorkerProvider");
	
	
	private int estimatedSize;
    private int batchSize;
    private final Iterator<NodeRef> iterator;
	
	
	public ClassementWorkerProvider(List<NodeRef> noderefs, int batchSize) {

        this.batchSize = batchSize;
        this.estimatedSize = noderefs.size();
        this.iterator = noderefs.iterator();
    }

	   @Override
	    public int getTotalEstimatedWorkSize() {
	        return this.estimatedSize;
	    }

	    @Override
	    public Collection<NodeRef> getNextWork() {
	        if (vmShutdownListener.isVmShuttingDown()) {
	            return Collections.emptyList();
	        }

	        Collection<NodeRef> items = new ArrayList<>(batchSize);
	        while (items.size() < batchSize && iterator.hasNext()) {
	            items.add(iterator.next());
	        }
	        return items;
	    }

}
