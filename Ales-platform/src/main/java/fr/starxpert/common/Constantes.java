package fr.starxpert.commom;

public class Constantes {
	public static final String PARAM_TYPE_DOC_FICHIER = "fichier";
	
	 public static final String COMMA = ";";
	 public static final String DEFAULT_SEPARATOR = COMMA;
	 public static final String DOUBLE_QUOTES = "\"";
	 public static final String EMBEDDED_DOUBLE_QUOTES = "\"\"";
	 
	 public static final String NEW_LINE_UNIX = "\n";
	 public static final String NEW_LINE_WINDOWS = "\r\n";
	
	public static final String DOCUMENTS_FINANCIERS_NON_CLASSES = "Documents non classés";
	
}
