package fr.starxpert.classement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

import org.alfresco.service.Auditable;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.starxpert.bean.Classement;
import fr.starxpert.bean.Condition;
import fr.starxpert.bean.Correspondance;
import fr.starxpert.bean.Document;
import fr.starxpert.bean.PlanDeClassement;
import fr.starxpert.bean.ReturnGenericObject;
import fr.starxpert.jobs.ServiceCommon;
import fr.starxpert.model.InfosMails;



public class RecherchePlanDeClassement {
	
	private static Log logger = LogFactory.getLog(PlanClassement.class);
	
	private ServiceCommon serviceCommon;
	
	@Autowired
	private NodeService nodeService;
	
	@Autowired
	private NamespaceService namespaceService;
	

	public ReturnGenericObject getCheminClassement(Document document, InfosMails infosMails) {
		
		// Cet objet permet de recuperer toutes sortes de valeurs. 
		ReturnGenericObject returnGenericObjects = new ReturnGenericObject();
		
		//lecture du fichier json de configuration pour détermination du plan de classement adéquat au type document
		PlanDeClassement planClassement = serviceCommon.lectureFichierDeConfiguration(document,infosMails.getNodeReeDossierCsv());
		
		List<Correspondance> listeCorrespondance = planClassement.getCorrespondances();
		
		returnGenericObjects.setListeCorrespondance(listeCorrespondance);
		
		String docDestinationPath = null;
		
		Classement classementSelectonne = null;
		
		if(planClassement != null ) {
		
		List<Classement> listClassement = planClassement.getClassement();
				
		Map<Classement,Integer> listFinal   = new HashMap<Classement, Integer>();
		
		int i= 1;
		
		for( Classement classemnt: listClassement) {
			
			int conditionVerifTotal=0;
			
			Condition condition = classemnt.getCondition();
			
			List<Boolean> listVerifMetadata = verifierConditionMetadata(condition,document);
			List<Boolean> listVerifType = verifierConditionType(condition,document);
			List<Boolean> listVerifAspect = verifierConditionAspect(condition, document);
			 
			logger.debug("taille listVerifMetadata pour le classement:"+ i+ " est: "+listVerifMetadata.size());
			logger.debug("taille listVerifType pour le classement:"+ i+ " est: "+listVerifType.size());
			logger.debug("taille ListVerifAspects pour le classement:"+ i+ " est: "+listVerifAspect.size());
			 
			
			 conditionVerifTotal = listVerifMetadata.size()+listVerifType.size()+listVerifAspect.size();

			logger.debug("conditionVerifTotal pour le classement:"+ i+ " est: "+conditionVerifTotal);
			 
			// listFinal.put(classemnt.getDossierDestination().concat("-").concat(classemnt.getSite()),conditionVerifTotal);
			listFinal.put(classemnt,conditionVerifTotal);
			 
			 i++;
		}
		
		//recuperer le chemain de classement qui à + de true
		int max = Collections.max(listFinal.values());
	
			
			for (Entry<Classement, Integer> entry : listFinal.entrySet()) {
			    if (entry.getValue()==max) {
			    	classementSelectonne = entry.getKey();
			    }
			}
			
		logger.debug("path of classement: "+docDestinationPath);
		
		}	
		
		returnGenericObjects.setClassement(classementSelectonne);
		
	return 	returnGenericObjects;
	}
	
	
	public List<Boolean> verifierConditionType(Condition condition,Document document) {
		
		List<Boolean> verifType = new ArrayList<Boolean>();
		 //comparaison entre types dans le json et celle qui existe sur le doc
		List<String> TypesDoc = condition.getTypes();
		
		 QName type = nodeService.getType(document.getNodeRef());
         Collection<String> p =  namespaceService.getPrefixes(type.getNamespaceURI());
       
		 if(TypesDoc.contains(p.stream().findFirst().get()+":"+ type.getLocalName())) {
			 verifType.add(true);
	
		 }
		 
		return verifType;
		
	}
	
	
	public List<Boolean> verifierConditionAspect(Condition condition,Document document) {
		
		List<Boolean> verifAspectList = new ArrayList<Boolean>() ;
		 //comparaison entre Aspect dans le json et celle qui existe sur le doc
		List<String> listAspect = condition.getAspects();
		
		
		for(String aspectJson :listAspect) {
			
			logger.debug("list verfiAspect pour le Document "+document.getNodeRef());
			QName QnameAspect= serviceCommon.getQnameByMetadata(aspectJson);
			
//			String[] aspectNameArray = aspectJson.split(":");
//		    String prefix = aspectNameArray[0];
//			String uriNameSpace = namespaceService.getNamespaceURI(prefix);
//			QName QnameAspect = QName.createQName(uriNameSpace, aspectNameArray[1]);
//			
			logger.debug("QnameAspect "+QnameAspect);
			
			Boolean aspect =  nodeService.hasAspect(document.getNodeRef(), QnameAspect);
			logger.debug("aspect Value "+aspect);
			if(aspect) {
				verifAspectList.add(true);
				 
			 }
			 
		}
	
		return verifAspectList;
		
	}
	
	
	
	public  List<Boolean> verifierConditionMetadata(Condition condition,Document document) {
	
		// Map de condition sur les metadata
		Map<String, List<String>>	mapDesMetadata = condition.getMetadata();
		List<String>  valuesMetadatajson ;
		
		List<Boolean> listVerif = new ArrayList<Boolean>();
		
		//logger.debug("taille de metadata json file: "+mapDesMetadata.size());
	    //parcourir la map mapDesMetadata 
		
		for (Map.Entry mapentry : mapDesMetadata.entrySet()) {
	           //System.out.println("cle: "+mapentry.getKey());
	           String metadataNameJson = (String) mapentry.getKey();
//	           String[] metadataNameArray = metadataNameJson.split(":");
//	           String prefix = metadataNameArray[0];
//	           String uriNameSpace = namespaceService.getNamespaceURI(prefix);
//	       	   String nomMetadonnee = metadataNameArray[1]; 
//	       	QName QnameMetadataJson = QName.createQName(uriNameSpace, nomMetadonnee);
	           
	           QName QnameMetadataJson = serviceCommon.getQnameByMetadata(metadataNameJson);
	       	String valeurMetadonneeDocument = (String) document.getMetadonneesDocument().get(QnameMetadataJson);
	       // System.out.println("valeurMetadonneeDocument: "+valeurMetadonneeDocument);
	       // System.out.println("pour la métadonné dans le json:"+mapentry.getKey() +"la valeur est"+mapentry.getValue());
	        
	        valuesMetadatajson =(List<String>) mapentry.getValue();
	        if(valuesMetadatajson.contains(valeurMetadonneeDocument)) {
	        	listVerif.add(true);
	      
	        }
	      
	        }

		return listVerif;
	 }

	/**
	 * @return the serviceCommon
	 */
	public ServiceCommon getServiceCommon() {
		return serviceCommon;
	}

	/**
	 * @param serviceCommon the serviceCommon to set
	 */
	public void setServiceCommon(ServiceCommon serviceCommon) {
		this.serviceCommon = serviceCommon;
	}
	
	
	
	
}
