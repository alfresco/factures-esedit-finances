package fr.starxpert.classement;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.model.FileExistsException;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileNotFoundException;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.security.PermissionService;
import org.alfresco.service.cmr.site.SiteService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.sun.tools.xjc.model.Constructor;

import fr.starxpert.bean.Classement;
import fr.starxpert.bean.Document;
import fr.starxpert.bean.ReturnGenericObject;
import fr.starxpert.jobs.ServiceCommon;

import fr.starxpert.model.InformationsClassement;
import fr.starxpert.model.InfosMails;
import fr.starxpert.service.ArborescenceService;
import fr.starxpert.service.EmailService;
import fr.starxpert.service.ErreurClassement;
import fr.starxpert.service.GroupeService;
import fr.starxpert.service.TraiterMailService;


public class ClassementDocument {

	private static Log logger = LogFactory.getLog(ClassementDocument.class);
	
	@Autowired
	private SiteService siteService;
	
	@Autowired
	private NodeService nodeService;
	
	@Autowired
	private GroupeService groupeService;
	
	@Autowired
	private ServiceRegistry RegistryService;
	
	@Autowired
	private PermissionService permissionService;
	
	private RecherchePlanDeClassement recherchePlanDeClassement;
	
	private ArborescenceService arborescenceService;
	
	private ServiceCommon serviceCommon;
	
	private EmailService  emailService;
    
    private InformationsClassement infoClassement;
    
    private  ErreurClassement  erreurClassement;
    
    private TraiterMailService  traiterMailService;
 
	
	@Autowired
	private FileFolderService fileFolderService;
	
	public void classementGenerique(NodeRef nodeRef) throws NoSuchMethodException, SecurityException{
		
				
		InfosMails infosForSendingMail = serviceCommon.getInfosForSendingMails();
				
		// parcourir les documents sous sas d'entrée
		List<ChildAssociationRef> listDocs = nodeService.getChildAssocs(nodeRef);

		logger.debug("******************* Début du classement Generique**********");
		
		logger.debug("Nombre de documents recupéré : " + listDocs.size());

		// création d'un objet document pour chaque document récupére

		// On parcours les documents recupérés du sas
		for (ChildAssociationRef childAssociationRef : listDocs) {
			
			NodeRef nodeRefFolderDestination = null;
         
			Document document = new Document();
			
			Map<QName, Serializable> properties = nodeService.getProperties(childAssociationRef.getChildRef());
			
			document.setMetadonneesDocument(properties);
			document.setNodeRef(childAssociationRef.getChildRef());
			document.setNomDocument(childAssociationRef.getQName().getLocalName());
			document.setPathDoc(infosForSendingMail.getCheminCsv());
			
			// String pathDestinationDoc = recherchePlanDeClassement.getCheminClassement(document, infosForSendingMail);
			
			ReturnGenericObject returnGenericObjects = recherchePlanDeClassement.getCheminClassement(document, infosForSendingMail);
			
			Classement classement = returnGenericObjects.getClassement();
			
			if(classement != null) {
		
				String[] pathInfo = {classement.getDossierDestination(), classement.getSite()};
				
				logger.debug("-----------dossierDestination--------------- *"+pathInfo[0]);
				logger.debug("-----------site--------------- *"+pathInfo[1]);

				// création de l'arborescence
				nodeRefFolderDestination = arborescenceService.creerArborescence(pathInfo, document, infosForSendingMail);
							
				 // les erreurs dans la creation du plan de classement sont gérés dans la classe Arborescence
				 if(nodeRefFolderDestination != null) {

						//avant le déplacement verifier si le fichier à classer ne porte pas le mme nom qu'un autre déja déposé
				
						String safeName = serviceCommon.getSafeName(nodeRefFolderDestination, childAssociationRef.getQName().getLocalName());
						
							try {
								logger.debug("-----------récupération param script--------------- ");	
								// Definition des parametres pour le script
								Object[] params = new Object[] {nodeRefFolderDestination, document, returnGenericObjects.getListeCorrespondance()};
								Object[] services = new Object[] {this.groupeService, this.serviceCommon, this.RegistryService};
								executionScript(classement, params, services);
								
								// move
								fileFolderService.move(childAssociationRef.getChildRef(), nodeRefFolderDestination, safeName);
								
							} catch (FileExistsException | FileNotFoundException | ClassNotFoundException 
									| IllegalAccessException | IllegalArgumentException | InvocationTargetException | InstantiationException e) {
				
								logger.debug("erreur lors de déplacement du document " + e);
								e.printStackTrace();
								erreurClassement.traiterErreur(document, false, true, infosForSendingMail.getNodeReeDossierCsv());
				
							}
							
						} 
				 
				// dans le cas ou le plan de classement est introuvable 
				} else {
					
					logger.debug("Plan de classement introuvable ");
					erreurClassement.traiterErreur(document, false, true, infosForSendingMail.getNodeReeDossierCsv());
					
				}
			 					
		}
		
		
		// initialisation de la date de dernier envoi
		if(emailService.getDateDernierEnvoi() == null) {
			emailService.setDateDernierEnvoi(serviceCommon.getDateNow());
		}	

		long diff = serviceCommon.getDateNow().getTime() - emailService.getDateDernierEnvoi().getTime();
		
		long hoursDiff = TimeUnit.MILLISECONDS.toHours(diff);
				
		double intervalle = hoursDiff/24.0;
		
		logger.debug("intervalle ecoulé depuis le dernier mail (en jours) : " + intervalle);
		
		if(intervalle >= Double.parseDouble(infosForSendingMail.getPeriodeLancement())) {
		
			traiterMailService.rechercheConditionEnvoiMail(infosForSendingMail);
		  
		}			
	}
	
	
	public void executionScript(Classement classement, Object[] params, Object[] services) 
			throws ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, InstantiationException {
		
		logger.debug("---------------------------DEBUT METHODE executionScript-------------------------------- ");
		logger.debug("---------------------------params SIZE-------------------------------- "+ params.length);
		logger.debug("---------------------------params services SIZE-------------------------------- "+ services.length);
		
		
		// Recuperation de la classe a partir de son nom
		Class<?> scriptClasse = Class.forName(classement.getScript());
		
		// Instanciation de la classe
		Object instance = scriptClasse.getDeclaredConstructor().newInstance();
		
		// Recuperation de la methode a executer
		Method executeMethode = scriptClasse.getDeclaredMethod("execute", Object[].class);
		
		Method injectionServiceMethod = scriptClasse.getDeclaredMethod("injectionServices", Object[].class);
		
		// On injecte les services dont on a besoin
		if(services != null && services.length > 0) {
			injectionServiceMethod.invoke(instance, new Object[] { services });
		}
			
		// Execution de la methode
		executeMethode.invoke(instance, new Object[] { params });
		

		
		
		
		
	}

	

	/**
	 * @return the recherchePlanDeClassement
	 */
	public RecherchePlanDeClassement getRecherchePlanDeClassement() {
		return recherchePlanDeClassement;
	}

	/**
	 * @param recherchePlanDeClassement the recherchePlanDeClassement to set
	 */
	public void setRecherchePlanDeClassement(RecherchePlanDeClassement recherchePlanDeClassement) {
		this.recherchePlanDeClassement = recherchePlanDeClassement;
	}

	/**
	 * @return the arborescenceService
	 */
	public ArborescenceService getArborescenceService() {
		return arborescenceService;
	}

	/**
	 * @param arborescenceService the arborescenceService to set
	 */
	public void setArborescenceService(ArborescenceService arborescenceService) {
		this.arborescenceService = arborescenceService;
	}

	/**
	 * @return the serviceCommon
	 */
	public ServiceCommon getServiceCommon() {
		return serviceCommon;
	}

	/**
	 * @param serviceCommon the serviceCommon to set
	 */
	public void setServiceCommon(ServiceCommon serviceCommon) {
		this.serviceCommon = serviceCommon;
	}


	/**
	 * @return the infoClassement
	 */
	public InformationsClassement getInfoClassement() {
		return infoClassement;
	}

	/**
	 * @param infoClassement the infoClassement to set
	 */
	public void setInfoClassement(InformationsClassement infoClassement) {
		this.infoClassement = infoClassement;
	}



	/**
	 * @return the erreurClassement
	 */
	public ErreurClassement getErreurClassement() {
		return erreurClassement;
	}



	/**
	 * @param erreurClassement the erreurClassement to set
	 */
	public void setErreurClassement(ErreurClassement erreurClassement) {
		this.erreurClassement = erreurClassement;
	}



	/**
	 * @param traiterMailService the traiterMailService to set
	 */
	public void setTraiterMailService(TraiterMailService traiterMailService) {
		this.traiterMailService = traiterMailService;
	}

	/**
	 * @param emailService the emailService to set
	 */
	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}
	
	

}
