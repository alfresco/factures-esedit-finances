package fr.starxpert.classement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.starxpert.bean.Classement;
import fr.starxpert.bean.Condition;
import fr.starxpert.bean.Correspondance;
import fr.starxpert.bean.PlanDeClassement;

import fr.starxpert.jobs.ServiceCommon;

public class PlanClassement {

	private static Log logger = LogFactory.getLog(PlanClassement.class);

	private Map<String, Map<String, Map<String, String>>> listeDesCorrespondances = new HashMap<String, Map<String, Map<String, String>>>();

	private ServiceCommon serviceCommon;

	public PlanDeClassement creerPlanClassement(JsonNode parser, ObjectMapper mapper) {

		Map<String, Map<String, String>> map = null;
		
		PlanDeClassement planDeClassement = new PlanDeClassement();
		
		List<Classement> classementList = new ArrayList<Classement>();
		
		List<Correspondance> correspondanceList = new ArrayList<Correspondance>();

		// On lit le contenu de la clé classement
		for (JsonNode conditionClassement : parser.path("classement")) {

			Classement objetClassement = new Classement();
			Condition objetCondition = new Condition();

			// classement:dossier
			String dossier = conditionClassement.path("dossier").asText();
			
			//logger.debug("le dossier est:  " + dossier);
			
			objetClassement.setDossierDestination(dossier);

			// classement:site
			String site = conditionClassement.path("site").asText();
			
			//logger.debug("le site est:  " + site);
			
			objetClassement.setSite(site);

			// classement:condition
			JsonNode conditions = conditionClassement.path("conditions");

			// condition:metadata
			JsonNode metadata = conditions.path("metadata");
			
			try {
				
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				
				Map<String, List<String>> mapMetadata = mapper.readValue(metadata.toString(),
						new TypeReference<Map<String, List<String>>>() {
						});
				
				//logger.debug("mapMetadata size:" + mapMetadata.size());

				objetCondition.setMetadata(mapMetadata);

			} catch (Exception e) {
				e.printStackTrace();

			}

			// condition:types
			JsonNode types = conditions.path("type");
			List<String> listeType = new ArrayList<>();
			
			//logger.debug("type:  " + types);
			
			Iterator<JsonNode> itr = types.elements();
			
			while (itr.hasNext()) {
				JsonNode type = itr.next();
				listeType.add(type.textValue());
			}
			
			objetCondition.setTypes(listeType);
			
			//logger.debug("sizeTypes:  " + listeType.size());

			// condition:aspect
			JsonNode aspects = conditions.path("aspect");
			List<String> listeAspect = new ArrayList<>();
			
			//logger.debug("aspect:  " + aspects);
			
			Iterator<JsonNode> itr2 = aspects.elements();
			
			while (itr2.hasNext()) {
				
				JsonNode aspect = itr2.next();
				listeAspect.add(aspect.textValue());
			}
			
			objetCondition.setAspects(listeAspect);
			
			//logger.debug("sizeAspects:  " + listeAspect.size());

			objetClassement.setCondition(objetCondition);

			// classement:dossierErreur
			String dossierErreur = conditionClassement.path("dossierErreur").asText();
			objetClassement.setDossierErreur(dossierErreur);
			//logger.debug("dossierErreur:  " + dossierErreur);

			// classement:script
			String script = conditionClassement.path("script").asText();
			objetClassement.setScript(script);
			//logger.debug("script:  " + script);

			// PlanDeClassement:classement:
			classementList.add(objetClassement);

		}

		// VOIR COMMENT ON PEUT MODIFIER LE CODE POUR PRENDRE EN COMPTE
		// LA MISE A JOUR DES FICHIERS DE CONRRESPONDANCES ODS
		// On lit le contenu de la clé correspondance
		for (JsonNode correspondances : parser.path("correspondance")) {

			Correspondance objetCorrespondance = new Correspondance();

			// correspondance:metadonnee
			String metadonnee = correspondances.path("metadonnee").asText();
			objetCorrespondance.setNommetadonnee(metadonnee);

			// correspondance:nomFichier
			String nomFichier = correspondances.path("nomFichier").asText();
			objetCorrespondance.setNomFichier(nomFichier);

			// correspondance:colonne
			String colonne = correspondances.path("colonne").asText();
			objetCorrespondance.setNumeroColonne(colonne);

			// condition pour la lecture du fichier de correspondance une seul fois
			if (listeDesCorrespondances.get(nomFichier) == null) {
				
				Map<String, Map<String, String>> tableCorrespondance = serviceCommon.getCorrespondance(nomFichier);
				listeDesCorrespondances.put(nomFichier, tableCorrespondance);
				
			}

			objetCorrespondance.setTableCorrespondance(listeDesCorrespondances.get(nomFichier));
            
			correspondanceList.add(objetCorrespondance);
		}

		// planDeClassement
		planDeClassement.setClassement(classementList);
		planDeClassement.setCorrespondances(correspondanceList);

		return planDeClassement;
	}

	/**
	 * @return the serviceCommon
	 */
	public ServiceCommon getServiceCommon() {
		return serviceCommon;
	}

	/**
	 * @param serviceCommon the serviceCommon to set
	 */
	public void setServiceCommon(ServiceCommon serviceCommon) {
		this.serviceCommon = serviceCommon;
	}

}
