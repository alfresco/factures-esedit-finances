package fr.starxpert.jobs;

import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class ClassementJob implements Job {
	
	private static Log logger = LogFactory.getLog(ClassementJob.class);

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		  JobDataMap jobData = context.getJobDetail().getJobDataMap();
	        
	        // Extract the Job executer to use
	        Object executerObj = jobData.get("classementJobExecutor");
	        
	        if (executerObj == null || !(executerObj instanceof ClassementJobExecutor)) {
	            throw new AlfrescoRuntimeException("ClassementDroitsJob data must contain valid 'Executer' reference");
	        }

	        // On caste le executerJob que l'on a récupéré
	        final ClassementJobExecutor jobExecutor = (ClassementJobExecutor) executerObj;

	        // Execution du job
	        AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<Object>() {
	            public Object doWork() throws Exception {
	                jobExecutor.execute();
	                return null;
	            }
	        }, AuthenticationUtil.getSystemUserName());

	}

}
