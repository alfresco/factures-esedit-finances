package fr.starxpert.jobs;

import java.io.File;


import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import java.util.Map;


import org.alfresco.model.ContentModel;
import org.alfresco.repo.nodelocator.NodeLocatorService;
import org.alfresco.service.ServiceRegistry;

import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.ContentIOException;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;

import org.alfresco.service.cmr.search.ResultSet;

import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jopendocument.dom.spreadsheet.MutableCell;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.starxpert.bean.Document;
import fr.starxpert.bean.PlanDeClassement;
import fr.starxpert.classement.PlanClassement;
import fr.starxpert.model.InformationsClassement;
import fr.starxpert.model.InfosMails;
import fr.starxpert.service.ErreurClassement;



public class ServiceCommon {

	private static Log logger = LogFactory.getLog(ServiceCommon.class);

	private ServiceRegistry serviceRegistry;

	private PlanClassement planClassement;
	
	private ErreurClassement erreurClassement;

	@Autowired
	private NodeService nodeService;

	@Value("${temp.file.reader}")
	private String tempFileReader;

	@Autowired
	private SearchService searchService;

	@Autowired
	private NamespaceService namespaceService;

	@Value("${chemin.fichiers.configuration.correspondance}")
	private String fichierConfigurationCorrespondance;

	@Value("${configurationClassement.file.path}")
	private String configurationClassementFilePath;
	
	
	@Value("${configurationClassementEnErreur.file.path}")
	private String configurationClassementEnErreurFilePath;
	
	

	//private NodeRef nodeFolder;
	
	@Autowired
	private NodeLocatorService nodeLocatorService;

	//Map<String, Map<String, String>> mapCorrespondance = new HashMap<String, Map<String, String>>();

	public NodeRef getNodeByPath(String path) {
		
		String query = "PATH:\"" + path + "\"";
		
		NodeRef result = null;
		ResultSet resultSet = null;
		
		try {
			
			resultSet = searchService.query(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, SearchService.LANGUAGE_FTS_ALFRESCO, query);
			
			if (resultSet != null && resultSet.length() > 0) {
				
				result = resultSet.getNodeRef(0);
				
			} else {
				return null;
			}

		} catch (NullPointerException e) {
			e.printStackTrace();
			return null;
		}
		
		return result;
	}

	public NodeRef nodeRefChemin(String path) {

		Map<String, Serializable> params = new HashMap<>();
		params.put("query", path);
		NodeRef nodeRef = serviceRegistry.getNodeLocatorService().getNode("xpath", null, params);
		return nodeRef;

	}

	public NodeRef createFolder(NodeRef rootRef, String folderName) {
		NodeRef parentFolderNodeRef = rootRef;

		// Create Node
		QName associationType = ContentModel.ASSOC_CONTAINS;
		NodeRef childRef = serviceRegistry.getNodeService().getChildByName(rootRef, associationType, folderName);
		
		if (childRef != null) {
			return childRef;
		}
		
		QName associationQName = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, QName.createValidLocalName(folderName));
		
		QName nodeType = ContentModel.TYPE_FOLDER;
		
		Map<QName, Serializable> nodeProperties = new HashMap<QName, Serializable>();
		nodeProperties.put(ContentModel.PROP_NAME, folderName);

		ChildAssociationRef parentChildAssocRef = serviceRegistry.getNodeService().createNode(parentFolderNodeRef, associationType, associationQName, nodeType, nodeProperties);

		return parentChildAssocRef.getChildRef();

	}
	
	
	public NodeRef createNode(String name, NodeRef parentFolderNodeRef, QName type) {

		QName associationType = ContentModel.ASSOC_CONTAINS;
	    NodeRef childRef = nodeService.getChildByName(parentFolderNodeRef, associationType, name);
		 if (childRef != null) {
		      return childRef;
		 }

		Map<QName, Serializable> properties = new HashMap<>();
		properties.put(ContentModel.PROP_NAME, name);

		QName associationQName = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI,
				QName.createValidLocalName(name));

		ChildAssociationRef parentChildAssocRef = serviceRegistry.getNodeService().createNode(parentFolderNodeRef,
				associationType, associationQName, type, properties);

		return parentChildAssocRef.getChildRef();
	}
	

	public QName getQnameByMetadata(String metadata) {

		logger.debug("metadata : " + metadata);
		String[] array = metadata.split(":");
		String prefix = array[0];
		String uriNameSpace = namespaceService.getNamespaceURI(prefix);
		QName qnameMetadata = QName.createQName(uriNameSpace, array[1]);
		logger.debug("qnameMetadata : " + qnameMetadata);
		return qnameMetadata;
	}



	

	public Map<String, Map<String, String>> getCorrespondance(String nomFichier) {

		logger.debug("--------------------DEBUT METHODE GET CORRESPONDANCE---------------");
		

		Map<String, Map<String, String>> listCorrespondanceData = new HashMap<String, Map<String, String>>();

		String tempFilename = null;

		if (tempFileReader != null) {
			tempFilename = tempFileReader + "/tempeReader.ods";
		} else {
			tempFilename = "tempeReader.ods";
		}

		File file = new File(tempFilename);

		NodeRef nodeRefDataDictionary = getChildNodeRef(getCompanyHome(), "Data Dictionary");
		NodeRef nodeConfigurationNodeRef = getChildNodeRef(nodeRefDataDictionary, "Configurations");
		NodeRef nodeCorrespondanceNodeRef = getChildNodeRef(nodeConfigurationNodeRef, "Correspondances");

		NodeRef nodeFichierCorrespondance = getChildNodeRef(nodeCorrespondanceNodeRef, nomFichier);
						
	  // a modifier le chemin
		//NodeRef nodeFichierCorrespondance = getNodeByPath(fichierConfigurationCorrespondance + nomFichier);
		logger.debug("path fichier de correspondance:" + nodeFichierCorrespondance);
		ContentReader reader = serviceRegistry.getContentService().getReader(nodeFichierCorrespondance, ContentModel.PROP_CONTENT);

		try {
			Files.copy(reader.getContentInputStream(), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (ContentIOException | IOException e1) {
         logger.error("erreur lors de la lecture des fichiers de correspondances: "+ e1);
			
			
		}
		// Sheet sheet;
		try {
          
		if(file != null) {	
			int sheetNumber = SpreadSheet.createFromFile(file).getSheetCount();

			for (int i = 0; i < sheetNumber; i++) {

				final Sheet sheet = SpreadSheet.createFromFile(file).getSheet(i); // Récupération de l'onglet souhaité

				// Get row count and column count
				int nColCount = sheet.getColumnCount();
				int nRowCount = sheet.getRowCount();

				// Iterating through each row of the selected sheet
				MutableCell celleCode = null;
				MutableCell celleLibelle = null;
				Map<String, String> mapLigne = null;

				for (int nRowIndex = 1; nRowIndex < nRowCount; nRowIndex++) {
					celleCode = sheet.getCellAt(0, nRowIndex);
					mapLigne = new HashMap<String, String>();

					for (int nColIndex = 1; nColIndex < nColCount; nColIndex++) {

						mapLigne.put(Integer.toString(nColIndex), sheet.getCellAt(nColIndex, nRowIndex).getValue().toString());
					//	logger.debug("mapLigne : " + Integer.toString(nColIndex) + ":" + sheet.getCellAt(nColIndex, nRowIndex).getValue().toString());

					}

					listCorrespondanceData.put(celleCode.getValue().toString(), mapLigne);
				}

//				listCorrespondanceData.forEach((key, value) -> {
//					// System.out.println(key+" |listCorrespondanceData| "+value);
//				});
			}
			
		}
		} catch (IOException e) {
			 logger.error("erreur lors de la lecture des fichiers de correspondances: "+ e);
		} finally {
			file.delete();
		}

		return listCorrespondanceData;
	}
	
	public InfosMails getInfosForSendingMails() {
		
		InfosMails infosMails = new InfosMails();

		// Ne recupere pas les infos du mail
		Map<String, String> infoEnvoiMail = lectureFichierConfigurationEnvoiMail();

		// 
		String periodeLancement = infoEnvoiMail.get("periode");
		infosMails.setPeriodeLancement(periodeLancement);

		String cheminCsv = infoEnvoiMail.get("cheminDestinationCSV");
		logger.debug("****cheminDestinationCSV"+ cheminCsv);
		infosMails.setCheminCsv(cheminCsv);

		String emailDestination = infoEnvoiMail.get("emailDestination");
		infosMails.setEmailDestination(emailDestination);

		String[] tabCheminParentCsv = cheminCsv.split("/");

		// Recuperation du dossier de dépot des csv : Le point de départ est l'entrepot
		NodeRef nodeRefRacine = getCompanyHome();

		for (int i = 0; i < tabCheminParentCsv.length; i++) {
		   
			if(!tabCheminParentCsv[i].equals("company_home")) {
				logger.debug("its true");

			NodeRef nodeRefParentCSV = getChildNodeRef(nodeRefRacine, tabCheminParentCsv[i]);
			

			// Si le dossier n'existe pas alors on le crée
			if (nodeRefParentCSV == null  ) {
				

				nodeRefParentCSV = createFolder(nodeRefRacine, tabCheminParentCsv[i]);

			}

			nodeRefRacine = nodeRefParentCSV;

		
		
		infosMails.setNodeReeDossierCsv(nodeRefRacine);
	    }
		
		}
		
		return infosMails;
	}
	 
	
	public  Map<String, String> lectureFichierConfigurationEnvoiMail() {
		 
		 Map<String, String> infosEnvoiMail  = new HashMap<String, String>();
		 
		 
         // Ne fonctionne pas	  
	     NodeRef nodeFichierConfigEmailJson = getNodeByPath(configurationClassementEnErreurFilePath);
	  	 
	  if(nodeFichierConfigEmailJson != null) {
		  
		  final ObjectMapper mapper = new ObjectMapper();
		
		  try {
		  
			ContentReader reader = serviceRegistry.getContentService().getReader(nodeFichierConfigEmailJson,
					ContentModel.PROP_CONTENT);
			
			JsonNode parser = mapper.readTree(reader.getContentInputStream());
				
			String periode = parser.path("periode").asText();
			String emailDestination = parser.path("emailDestination").asText();
			String cheminDestinationCSV = parser.path("cheminDestinationCSV").asText();
			
			
			infosEnvoiMail.put("cheminDestinationCSV", cheminDestinationCSV);
			infosEnvoiMail.put("emailDestination", emailDestination);
			infosEnvoiMail.put("periode", periode);
			
			
		  
		  } catch (ContentIOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		}
	  
	  return infosEnvoiMail;
	 
	 }

	
	
	// "Data Dictionary"
	public NodeRef getChildNodeRef(NodeRef parentFolder, String folderName) {
		
		NodeRef currentFolder = nodeService.getChildByName(parentFolder, ContentModel.ASSOC_CONTAINS, folderName);
		
		logger.debug("Current node  '" +folderName +"' : " + currentFolder);
		
		return currentFolder;
	}
	
	
	
	
	/*public NodeRef createFolder(NodeRef parent, String folderName) {
		return null;
		
	}*/
	
	
	
	
	

	public PlanDeClassement lectureFichierDeConfiguration(Document document, NodeRef csvFolder) {
		
		final ObjectMapper mapper = new ObjectMapper();

		PlanDeClassement plan = null;

		try {

			NodeRef nodeFichierConfigJson = getNodeByPath(configurationClassementFilePath);

			// Controler la date de derniere modification et la comparer avec la date de
			// modificiation enregistrée
			// Si les dates sont différentes alors on va relier le fichier json sinon on ne
			// fait rien

			if(nodeFichierConfigJson != null) {
			
				ContentReader reader = serviceRegistry.getContentService().getReader(nodeFichierConfigJson,
						ContentModel.PROP_CONTENT);
				
	
				JsonNode parser = mapper.readTree(reader.getContentInputStream());
	
				plan = planClassement.creerPlanClassement(parser, mapper);
			  
			}
			
		
		} catch (JsonMappingException e) {
           
		   logger.error("exception lors du mapping du fichier Json" + e);
		   
		   erreurClassement.traiterErreur(document, false, true, csvFolder);
           
		} catch (IOException e) {
			
			logger.error("exception lors de  la lecture du fichier Json" + e);
		
		  erreurClassement.traiterErreur(document, false, true, csvFolder);
			
//			document.setMetadonnesManquante(null);
//			
//			 erreurClassement.traiterErreur(document, false, true);
//			

		}

		return plan;

	}
	
	
	

	 public String getSafeName(NodeRef folder, String fileName) {

	      String extention = (fileName.lastIndexOf('.') > 0) ? fileName.substring(fileName.lastIndexOf('.'), fileName.length()) : "";
	      String name = (fileName.lastIndexOf('.') > 0) ? fileName.substring(0, fileName.lastIndexOf('.')) : fileName;
	      int count = 0;

	      while (nodeService.getChildByName(folder, ContentModel.ASSOC_CONTAINS, fileName) != null) {

	          String index = (String.valueOf(++count).length() > 1) ? "_" + count : "_0" + count;
	          fileName = name + index + extention;
	      }

	      return fileName;

	  }
	 

	 public Date getDateNow() {
	    
	   Date dateEnvoiMail = null;
	
		try {	
			 
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	     
		    Calendar calendar = Calendar.getInstance();
	
	        Date dateObj = calendar.getTime();
	     
	        String formattedDate = sdf.format(dateObj);
	        
			dateEnvoiMail = sdf.parse(formattedDate);
			
		 } catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        
       return dateEnvoiMail;
	 }
	 
	 
	 public String getCSVname() {
		 
		  SimpleDateFormat dtf = new SimpleDateFormat("dd-MM-yyyy");
	      
		  Calendar calendar = Calendar.getInstance();

	      Date dateObj = calendar.getTime();
	      
	      String formattedDate = dtf.format(dateObj);
	      
	      String csvName = "nonclasses_".concat(formattedDate).concat(".csv");
	      
	      return csvName;
	   }
	
	/**
	 * Returns the NodeRef of "Company Home"
	 *
	 * @return
	 */
	public NodeRef getCompanyHome() {
		return nodeLocatorService.getNode("companyhome", null, null);
	}
	
	
	
	

	public void setServiceRegistry(ServiceRegistry serviceRegistry) {
		this.serviceRegistry = serviceRegistry;
	}

	/**
	 * @param nodeService the nodeService to set
	 */
	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}

	/**
	 * @return the planClassement
	 */
	public PlanClassement getPlanClassement() {
		return planClassement;
	}

	/**
	 * @param planClassement the planClassement to set
	 */
	public void setPlanClassement(PlanClassement planClassement) {
		this.planClassement = planClassement;
	}

	/**
	 * @return the erreurClassement
	 */
	public ErreurClassement getErreurClassement() {
		return erreurClassement;
	}

	/**
	 * @param erreurClassement the erreurClassement to set
	 */
	public void setErreurClassement(ErreurClassement erreurClassement) {
		this.erreurClassement = erreurClassement;
	}

	

}
