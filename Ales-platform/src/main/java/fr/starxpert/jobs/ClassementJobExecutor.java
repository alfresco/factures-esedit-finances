package fr.starxpert.jobs;

import java.util.List;


import org.alfresco.repo.batch.BatchProcessor;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.transaction.TransactionService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import fr.starxpert.classement.ClassementDocument;
import fr.starxpert.model.AlesDocumentModel;
import fr.starxpert.service.AnalyseDepotService;
import fr.starxpert.workers.ClassementWorker;
import fr.starxpert.workers.ClassementWorkerProvider;

public class ClassementJobExecutor {
	
	private static Log logger = LogFactory.getLog(ClassementJobExecutor.class);

	
	
	private AnalyseDepotService analyseDepotService;
	
	private ClassementDocument classementDocument;
	
	@Autowired
	private TransactionService transactionService;
		
	@Value("${suppression.droits.batch.size}")
	private int batchSize;
	
	@Value("${suppression.droits.worker.threads}")
	private int workerThreads;

	public void execute() {
		
		logger.debug("[ALES RECLASSEMENT] INITIALISATION DU JOB ");
		
	    List<NodeRef> listNodeRef = analyseDepotService.getSasAlesFolders();
	    
	    
		
	    if (listNodeRef.size() == 0)
			return;
		// Les batchProcessor gerent le lancement de traitement qui sont tres long.
		// Il prends en entré une collection (ici les documents) et il itere sur elle
		BatchProcessor<NodeRef> processor = new BatchProcessor<NodeRef>(
		"batchClassement",transactionService.getRetryingTransactionHelper(), 
		new ClassementWorkerProvider(listNodeRef, batchSize),
		workerThreads, 
		batchSize, 
		null, 
		null, 
		batchSize);
		
		processor.process(new ClassementWorker(classementDocument), true);	
	}



	/**
	 * @return the analyseDepotService
	 */
	public AnalyseDepotService getAnalyseDepotService() {
		return analyseDepotService;
	}

	/**
	 * @param analyseDepotService the analyseDepotService to set
	 */
	public void setAnalyseDepotService(AnalyseDepotService analyseDepotService) {
		this.analyseDepotService = analyseDepotService;
	}



	/**
	 * @return the classementDocument
	 */
	public ClassementDocument getClassementDocument() {
		return classementDocument;
	}



	/**
	 * @param classementDocument the classementDocument to set
	 */
	public void setClassementDocument(ClassementDocument classementDocument) {
		this.classementDocument = classementDocument;
	}
	
	

}
