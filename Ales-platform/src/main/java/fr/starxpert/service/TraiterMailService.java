package fr.starxpert.service;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.starxpert.jobs.ServiceCommon;
import fr.starxpert.model.InformationsClassement;
import fr.starxpert.model.InfosMails;



public class TraiterMailService {
	
	private static Log logger = LogFactory.getLog(TraiterMailService.class);
	
	private ServiceCommon serviceCommon;
	
	@Autowired
	private NodeService nodeService;
	
	private EmailService  emailService;

	private ErreurClassement  erreurClassement;
	
	public void rechercheConditionEnvoiMail(InfosMails infosForSendingMail) {
		
		
	    
	      logger.info("nodeRefParentCSV: "+infosForSendingMail.getNodeReeDossierCsv());
	    
	      //parcourir tous les csv creer sous le rep d'erreur
	      List<ChildAssociationRef> listCsv = nodeService.getChildAssocs(infosForSendingMail.getNodeReeDossierCsv());
	       
	      InformationsClassement  informationsClassement  = new InformationsClassement();
	      
	      for(ChildAssociationRef childAssociationRef : listCsv) {
	    	  
	    	  String csvName = childAssociationRef.getQName().getLocalName();
	    	  String dateCreationCSV =   csvName.substring(csvName.indexOf("_")+1, csvName.indexOf("."));
	    	
	    	  SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	    	  Date datecsvFile = null;
	    	  
			try {
				
				datecsvFile = sdf.parse(dateCreationCSV);
				
			} catch (ParseException e) {
				
				logger.error("erreur servenue lors du parsing de la date");
			}
			
	    	  Date dateNow = serviceCommon.getDateNow();
	    	  
	    	  
	    	  // Verification des conditions pour l'envoie du mail
	    	  Period period = Period.between(datecsvFile.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
	    			          dateNow.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
	    	  
	    	  logger.debug("period.getDays(): "+ period.getDays());
	    	  
	    	  if(period.getDays() >= Integer.parseInt(infosForSendingMail.getPeriodeLancement())) {
	    		  
	    		  informationsClassement.setCheminFichierCSV(infosForSendingMail.getCheminCsv());
	    		  
	    		  informationsClassement.setNomFichierCsv("nonclasses_".concat(dateCreationCSV));
	    		  
	    		  int nbDoc = erreurClassement.getNbDocNonClasses("nonclasses_".concat(dateCreationCSV).concat(".csv"));
	    		  
	    		  informationsClassement.setNombreFichersErreurs(nbDoc);
	    		 
	    		  envoyerMailAlerte(informationsClassement,infosForSendingMail.getEmailDestination());
	    		 
	    		  break;
	    	  }
	    	 
	      }
	  
		
		
	}
	
	
	
	public void envoyerMailAlerte(InformationsClassement infoClassement,String emailDestination) {
		
		 Map<String, Serializable> templateArgs = new HashMap<>();
   	  
   	     templateArgs.put("cheminCSV",  infoClassement.getCheminFichierCSV());
   	 
   	     templateArgs.put("nomFichierCsv",  infoClassement.getNomFichierCsv());
   	  
     	 templateArgs.put("NbTotalDocNonClasses", infoClassement.getNombreFichersErreurs());
   	 
    	 emailService.send("VILLE ALES [ALERTE DOCUMENTS NON CLASSES]", emailDestination, "notification-mail.html.ftl", templateArgs);
    	 
    	 emailService.setDateDernierEnvoi(serviceCommon.getDateNow());
    	 
    	
	}



	/**
	 * @param serviceCommon the serviceCommon to set
	 */
	public void setServiceCommon(ServiceCommon serviceCommon) {
		this.serviceCommon = serviceCommon;
	}



	/**
	 * @param emailService the emailService to set
	 */
	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}



	/**
	 * @param erreurClassement the erreurClassement to set
	 */
	public void setErreurClassement(ErreurClassement erreurClassement) {
		this.erreurClassement = erreurClassement;
	}
	
	
	
	
}
