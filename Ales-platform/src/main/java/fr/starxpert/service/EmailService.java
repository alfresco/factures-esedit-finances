package fr.starxpert.service;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.alfresco.repo.action.executer.MailActionExecuter;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ActionService;
import org.alfresco.service.cmr.invitation.InvitationException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchService;
import org.apache.log4j.Logger;

import fr.starxpert.jobs.ServiceCommon;

public class EmailService {
	
	 /* logger */
    private final Logger logger = Logger.getLogger(EmailService.class);

   
    private ActionService actionService;
    
    private SearchService searchService;
    
    
    private ServiceCommon serviceCommon;
    
    private Date dateDernierEnvoi = null;
    
    private String mailFrom = "alfresco@alfresco.org";

    public void send(String subject, String recipient, String templateName, Map<String, Serializable> templateArgs)
    {
        try {
            String from = mailFrom;
            logger.debug("send email: ENTER");

            logger.debug("from = " +from);
            logger.debug("recipient = " +recipient);
            logger.debug("templateName = " +templateName);

            Action mailAction = actionService.createAction(MailActionExecuter.NAME);

            mailAction.setParameterValue(MailActionExecuter.PARAM_SUBJECT, subject);
            mailAction.setParameterValue(MailActionExecuter.PARAM_TO, recipient);

            NodeRef template = getEmailTemplateNodeRef(templateName);
            mailAction.setParameterValue(MailActionExecuter.PARAM_TEMPLATE, template);

            if(from !=null)
            {
                mailAction.setParameterValue(MailActionExecuter.PARAM_FROM, from);
            }

            Map<String, Serializable> templateModel = new HashMap<String, Serializable>();
            templateModel.put("args",(Serializable)templateArgs);
            mailAction.setParameterValue(MailActionExecuter.PARAM_TEMPLATE_MODEL,(Serializable)templateModel);

            actionService.executeAction(mailAction, null);
            
            //setter la date de dernier envoi 
            
            Date dateNow = serviceCommon.getDateNow();
            
            setDateDernierEnvoi(dateNow);
             
            dateDernierEnvoi = dateNow;
            
           setDateDernierEnvoi(dateDernierEnvoi);
            
            logger.debug("send email: LEAVE");
        
        } catch(Exception e) {
            logger.debug("send email: ERROR OCCURED FOR : " + recipient);
        }
        
    }
    
   
    private NodeRef getEmailTemplateNodeRef(String templateName) {

        String querySub = "+PATH:\"/app:company_home/app:dictionary/app:email_templates/cm:ville-ales/cm:" + templateName + "\"";

        ResultSet resultSet = searchService.query(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE,SearchService.LANGUAGE_LUCENE, querySub);

        if (resultSet.length()==0){
            logger.error("Template "+ querySub+" not found.");
            throw new InvitationException("Cannot find the email template!");
            //return null;
        }
        NodeRef template = resultSet.getNodeRef(0);

        return template;
    }

	/**
	 * @param actionService the actionService to set
	 */
	public void setActionService(ActionService actionService) {
		this.actionService = actionService;
	}

	/**
	 * @param searchService the searchService to set
	 */
	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}

	/**
	 * @return the dateDernierEnvoi
	 */
	public Date getDateDernierEnvoi() {
		
       return dateDernierEnvoi;
		
	}

	/**
	 * @param dateDernierEnvoi the dateDernierEnvoi to set
	 */
	public void setDateDernierEnvoi(Date dateDernierEnvoi) {
		this.dateDernierEnvoi = dateDernierEnvoi;
	}

	/**
	 * @param serviceCommon the serviceCommon to set
	 */
	public void setServiceCommon(ServiceCommon serviceCommon) {
		this.serviceCommon = serviceCommon;
	}
    
    
    

	

}
