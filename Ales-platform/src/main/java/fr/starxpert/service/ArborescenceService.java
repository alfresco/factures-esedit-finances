package fr.starxpert.service;




import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.security.PermissionService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.starxpert.bean.Correspondance;
import fr.starxpert.bean.Document;
import fr.starxpert.bean.PlanDeClassement;
import fr.starxpert.jobs.ServiceCommon;
import fr.starxpert.model.InfosMails;


public class ArborescenceService {
	
	private static Log logger = LogFactory.getLog(ArborescenceService.class);
	
	private ServiceCommon serviceCommon;
	
	private ErreurClassement erreurClassement;
	
	@Autowired
	private PermissionService permissionService;
	
	
	
	@Autowired
	private NodeService nodeService;
	
	Map<String, Map<String, String>> mapCorrespondance = new HashMap<String, Map<String, String>>();
	
	private NodeRef nodeFolder;
	
	public NodeRef creerArborescence(String[] pathInfo, Document doc, InfosMails infosMails)  {
		
		NodeRef nodeRefDestination = AuthenticationUtil.runAsSystem(() -> {
            // do your work
            return wrapperCreerArborescence(pathInfo,  doc,  infosMails);
		});
		
		return  nodeRefDestination;
		 
	}
	
	
	
	public NodeRef wrapperCreerArborescence(String[] pathInfo, Document doc, InfosMails infosMails)  {
		
		logger.debug("------ Le plan de classement sélectionné -----------: " + pathInfo[0]);
		
		logger.debug("************* creation Arboresence pour le document: " + doc.getNodeRef());
		
		// recupération du chemin du site ou l'on doit classer le document
		String chemin = "/app:company_home/st:sites/cm:" + pathInfo[1] + "/cm:documentLibrary";
		
		doc.setNomSite(pathInfo[1]);
		
		// ON recupere le noderef du documentLibrary
		NodeRef parentNode = serviceCommon.nodeRefChemin(chemin);
		
		// On recupere le chemin de classement renseigné dans le json
		String[] arbo = pathInfo[0].split("/");

		List<String> arboList = Arrays.asList(arbo);

		Map<QName, Serializable> properties = doc.getMetadonneesDocument();
		
		Map<String, String> metadataList = new HashMap<String, String>();
		
		if(parentNode != null && arboList != null ) {

			for (String niv : arboList) {
	
				String correspondanceValue = niv.substring(1, niv.length() - 1);
				
				// Si le niveau doit faire appel a une table de correspondance (symbole "<...>")
				if (niv.charAt(0) == '<') {
					
					logger.debug("le niveau contient <");
	
					PlanDeClassement planDeClassemnt = serviceCommon.lectureFichierDeConfiguration(doc, infosMails.getNodeReeDossierCsv());
					
					for (Correspondance correspondance : planDeClassemnt.getCorrespondances()) {
						
						if (correspondance.getNommetadonnee().equals(correspondanceValue)) {
							mapCorrespondance = serviceCommon.getCorrespondance(correspondance.getNomFichier());
							break;
						}
					}
	
					String codeMetadata = (String) nodeService.getProperty(doc.getNodeRef(), serviceCommon.getQnameByMetadata(correspondanceValue));
					
					logger.debug("niv : " + niv);
					
					metadataList.put(niv.substring(niv.indexOf(":") + 1, niv.length()-1), codeMetadata);
					
					if(!StringUtils.isBlank(codeMetadata)){
	
						for (Entry<String, Map<String, String>> entry : mapCorrespondance.entrySet()) {
							
							if (entry.getKey().equals(codeMetadata)) {
								
								for (Map.Entry mapLibelle : entry.getValue().entrySet()) {
									
									String libelle = (String) mapLibelle.getValue();
									
										nodeFolder = serviceCommon.createFolder(parentNode, libelle);
										clearPermissions(nodeFolder);
										parentNode = nodeFolder;
										
								}
								
								break;
		
							}
					 }
					
				    }else {
				    	
				    	// recuperation des metadonnées manquantes
				    	doc.setMetadonnesManquante(niv.substring(niv.indexOf(":") + 1, niv.length()-1));
				    	doc.setMetadataDoc(arboList);
				    	
				    	 erreurClassement.traiterErreur(doc, true, false, infosMails.getNodeReeDossierCsv());
				    	 parentNode = null;
				    	
						return parentNode;
						
				    }
	
			    // Si le niveau est construit directement a partir de la valeur de la metadonnée (symbole "[...]")	
				} else if (niv.charAt(0) == '[') {
	
					logger.debug("le niveau contient [ ");
					
					logger.debug("correspondanceValue : " + correspondanceValue);
	
					var entry = properties.entrySet().stream().filter(q ->
					
					q.getKey().getLocalName().equals(correspondanceValue.substring(correspondanceValue.indexOf(":") + 1, correspondanceValue.length()))
					
					).findFirst();
					
					logger.debug("entry : " + entry);
					
					metadataList.put(niv.substring(niv.indexOf(":") + 1, niv.length()-1), entry.get().getValue().toString());
					
					logger.debug("metadataList : " + metadataList);
				
					if (entry.isPresent()) {
						
						logger.debug("entry.get().getValue().toString() : " + entry.get().getValue().toString());
						
						 if (!StringUtils.isBlank(entry.get().getValue().toString())) {
							
								nodeFolder = serviceCommon.createFolder(parentNode, entry.get().getValue().toString());
								
								logger.debug("nodeFolder : " + nodeFolder);
								
								clearPermissions(nodeFolder);
							
								parentNode = nodeFolder;
								
								
						 }else {
							 
							 // recuperation des metadonnées manquantes
							 doc.setMetadonnesManquante(niv.substring(niv.indexOf(":") + 1, niv.length()-1));
							 doc.setMetadataDoc(arboList);
							 erreurClassement.traiterErreur(doc, true, false, infosMails.getNodeReeDossierCsv());
							 
							 parentNode = null;
							 
							 return parentNode;
							
						 }
					}
				
				}
			}
	
	}
	  return parentNode;

	}
	
	
	public void clearPermissions(NodeRef folderNodeRef) {
		
		// permissionService.clearPermission(folderNodeRef, null);
		// permissionService.deletePermissions(folderNodeRef);
		 permissionService.setInheritParentPermissions(folderNodeRef, false);
		 
	}
	
	
	
	/**
	 * @return the serviceCommon
	 */
	public ServiceCommon getServiceCommon() {
		return serviceCommon;
	}


	/**
	 * @param serviceCommon the serviceCommon to set
	 */
	public void setServiceCommon(ServiceCommon serviceCommon) {
		this.serviceCommon = serviceCommon;
	}


	/**
	 * @return the erreurClassement
	 */
	public ErreurClassement getErreurClassement() {
		return erreurClassement;
	}


	/**
	 * @param erreurClassement the erreurClassement to set
	 */
	public void setErreurClassement(ErreurClassement erreurClassement) {
		this.erreurClassement = erreurClassement;
	}


	
	
}
