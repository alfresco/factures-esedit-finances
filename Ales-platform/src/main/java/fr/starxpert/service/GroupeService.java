package fr.starxpert.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.security.AuthorityService;
import org.alfresco.service.cmr.security.AuthorityType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.starxpert.bean.Groupe;
import fr.starxpert.script.ScriptGestionDroits;

public class GroupeService {
	
	private static Log logger = LogFactory.getLog(ScriptGestionDroits.class);
	
	@Autowired
	private ServiceRegistry registryService;
	
	@Autowired
	private AuthorityService authorityService;

	@Autowired
	private NodeService nodeService;
	
	private final String PREFIX_GROUP = "GROUP_";
	
	Map<String, Groupe> listeGroupes = new HashMap<String, Groupe>();
	
	public GroupeService() {}
	
	/** 
	 * 
	 * @param listGroupsToCreate Map< CodeService, Map<IndexColonne, Label>>
	 */
	public void createGroupList(String service, Map<String, Map<String, String>> listGroupsToCreate, String column) {
		
		listeGroupes = new HashMap<String, Groupe>();
		
		Groupe groupeMainService = getGroup(PREFIX_GROUP + service.toUpperCase());
		

		
		if(groupeMainService == null) {
			logger.debug("groupeMainService null ");
			NodeRef groupNodeRef =  createGroupInTransaction(service.toUpperCase(), service, false, null);
			
			String authorityGroupName = (String) nodeService.getProperty(groupNodeRef,ContentModel.PROP_AUTHORITY_NAME);
			String label = (String) nodeService.getProperty(groupNodeRef,ContentModel.PROP_NAME);
		
			groupeMainService = new Groupe(groupNodeRef, authorityGroupName, label);
		}
		
		String parentGroupAuthority = groupeMainService.getAuthorityName();
		
		logger.debug("parentGroupAuthority" +parentGroupAuthority);

		listGroupsToCreate.entrySet().stream().forEach(e ->{
			
			String serviceName = e.getKey();
			String label = e.getValue().get(column);
			
			logger.debug("Authority group : " + PREFIX_GROUP + serviceName);
			logger.debug("is group exist  : " + isGroupExist(PREFIX_GROUP + serviceName));
			
			Groupe groupe = getGroup(PREFIX_GROUP + serviceName);
			
		
			 
			logger.debug("group : " + groupe);
			
			NodeRef groupNR = authorityService.getAuthorityNodeRef(PREFIX_GROUP + serviceName);
			
			logger.debug("groupNR : " + groupNR);
			
			
			if(groupe==null){
				
				logger.debug("group is null " +parentGroupAuthority);

			
				 if(!serviceName.isBlank() && !serviceName.isEmpty()){
					 
					NodeRef groupNodeRef =  createGroupInTransaction(PREFIX_GROUP +serviceName, label, true, parentGroupAuthority);
					
					listeGroupes.put(serviceName, new Groupe(groupNodeRef, PREFIX_GROUP+serviceName, label));
				 } 
			} else {
				
				logger.debug("group is not null " +parentGroupAuthority);
				
				listeGroupes.put(serviceName, groupe);
				
			}
		});
	}
		
	/**
	 * 
	 * @param groupName
	 * @param libelleGroupe
	 * @return
	 */
	private NodeRef createGroupInTransaction(String groupName, String libelleGroupe,  boolean isSubGroup, String parentGroupName) {

		NodeRef groupNodeRef = registryService.getRetryingTransactionHelper()
				.doInTransaction(new RetryingTransactionHelper.RetryingTransactionCallback<NodeRef>() {
					public NodeRef execute() throws Throwable {

						// set de la propriété "zone"
						Set<String> lstZone = new HashSet();
						lstZone.add(AuthorityService.ZONE_AUTH_ALFRESCO);
						lstZone.add(AuthorityService.ZONE_APP_DEFAULT);

						// creation du groupe

						String group = authorityService.createAuthority(AuthorityType.GROUP, groupName, libelleGroupe, lstZone);
						
						NodeRef groupNodeRef = authorityService.getAuthorityNodeRef(group);

						logger.debug("[ALES] groupe créé : " + group + " " + groupNodeRef.toString());
						
					
						if(isSubGroup) {
							authorityService.addAuthority(parentGroupName, group);

						//	NodeRef parentGroupNodeRef = authorityService.getAuthorityNodeRef(parentGroupName);
						//	nodeService.moveNode(groupNodeRef, parentGroupNodeRef, ContentModel.ASSOC_MEMBER, null);
						}
					

						return groupNodeRef;

					}
				}, false, true);

		return groupNodeRef;
	}
	
		
	/**
	 * 
	 * @param groupName : prefixé par GROUP_
	 * @return
	 */
	public Groupe getGroup(String groupName) {
		
		NodeRef groupNodeRef = authorityService.getAuthorityNodeRef(groupName);
		
		if(groupNodeRef != null) {
			
			String authorityGroupName = (String) nodeService.getProperty(groupNodeRef,ContentModel.PROP_AUTHORITY_NAME);
			String label = (String) nodeService.getProperty(groupNodeRef,ContentModel.PROP_NAME);
		
			return new Groupe(groupNodeRef, authorityGroupName, label);
			
		} else {
			
			return null;
			
		}
	}
	
	/**
	 * 
	 * @param groupeAuthorityName : prefixé par GROUP_
	 * @return
	 */
	public boolean isGroupExist(String groupeAuthorityName) {
		return authorityService.authorityExists(groupeAuthorityName);
	}
	
	
	
	
	
	
	public void createUsersofGroups(Map<String, Map<String, String>> listUsersToCreate, String column) {
		
		
		listUsersToCreate.entrySet().stream().forEach(e ->{
			
			String userName = e.getKey();
			String groupeName = e.getValue().get(column);
			
			logger.debug("userName to create : " + userName + " from group " + groupeName);
			
			String groupAuthorityName = authorityService.getName(AuthorityType.GROUP,groupeName);
			
			String userAuthorityName = authorityService.getName(AuthorityType.USER,userName);
			
			
			
			Set<String> authorityUser = authorityService.getAuthoritiesForUser(userName);
			
			//ajouter utilisateur si le groupe existe , l'utilisateur existe et l'utilisateur n'est pas affécté au groupe courant
		   if(isGroupExist(PREFIX_GROUP + groupeName) 
			  && authorityService.authorityExists(userName) 
			  && !authorityUser.contains(PREFIX_GROUP+groupeName)) {	
			
			   logger.debug(" it is true for affecting : "+userName  + " to "+ groupeName+ " group");
			  
			   authorityService.addAuthority(groupAuthorityName,userAuthorityName);
		   }
		});
		
	}
	
	
	public void deleteGroup() {
		
	}
	
	public Map<String, Groupe> getListeGroupes() {
		return listeGroupes;
	}

	public void setListeGroupes(Map<String, Groupe> listeGroupes) {
		this.listeGroupes = listeGroupes;
	}

}
