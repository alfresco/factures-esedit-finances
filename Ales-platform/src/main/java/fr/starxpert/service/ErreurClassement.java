package fr.starxpert.service;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.model.FileExistsException;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileNotFoundException;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.opencsv.CSVWriter;
import fr.starxpert.bean.Document;
import fr.starxpert.commom.Constantes;
import fr.starxpert.jobs.ServiceCommon;



public class ErreurClassement {
	
	private static Log logger = LogFactory.getLog(ErreurClassement.class);
	
	
	private ServiceCommon serviceCommon;
	
	private ServiceRegistry serviceRegistry;
	
	
	@Autowired
	private FileFolderService fileFolderService;
	

	@Autowired
	private NodeService nodeService;
	

  public boolean traiterErreur(Document document, boolean erreur_metadonnees, boolean erreur_grave, NodeRef csvFolder) {
	 
	 
	  logger.debug("traiterErreur");
	  String  csvName = serviceCommon.getCSVname();
	 
      Map<String, String> dataList = getMetadonneesDocument(document.getNodeRef(),document.getMetadataDoc());
      
      List<String[]> listData = new ArrayList<>();
      
      ContentWriter writer = null;	
      
      
     //récuperer le chemin du fichier nonclasses-datedujour.csv à partir du json
      
    //  NodeRef nodeRefRacine  =  recupererNodeParentcsv(document);
      NodeRef nodeRefRacine  =  csvFolder;
      
   
      logger.debug("nodeRefParentCSV: "+nodeRefRacine);
     
      NodeRef nodeCsvFile = serviceCommon.createNode(csvName,nodeRefRacine, ContentModel.TYPE_CONTENT);
      
      logger.debug("nodeCsvFile: "+nodeCsvFile);
		
      File file = new File("tempFile.csv");
      
    try {
    
    	 ContentReader reader = serviceRegistry.getContentService().getReader(nodeCsvFile, ContentModel.PROP_CONTENT);
    
	  
//      FileWriter outputfile = new FileWriter(file, true);
//	  
//      CSVWriter Csvwriter = new CSVWriter(outputfile, ';',
//                CSVWriter.NO_QUOTE_CHARACTER,
//                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
//                CSVWriter.DEFAULT_LINE_END);
      
      DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
      LocalDateTime now = LocalDateTime.now();  
      
      String dateTraitement = dtf.format(now);
     
	 
//      if(reader == null) {
//	  
//    	  String[] header = {"Nom Document" , "Nom Site" , "Chemin" , "Node Ref" , "date erreur" ,"erreur metadonnees" , "erreur grave" ,
//                   "metadonnee manquante" , "exercice" , "dateEmissionY" , "typeDocument" , "codeBudget" , 
//                   "codeService" , "nomTitulaire" , "numeroMarche" , "nomFournisseur", "codeServiceEmetteur"};
//	      
//    	  String[] info = {document.getNomDocument() ,document.getNomSite(), document.getPathDoc(), document.getNodeRef().toString(),dateTraitement
//    			  ,String.valueOf(erreur_metadonnees),String.valueOf(erreur_grave),document.getMetadonnesManquante()
//    			  ,getValue(dataList.get("exercice")) ,getValue(dataList.get("dateEmissionY")),getValue(dataList.get("typeDocument"))
//				  ,getValue(dataList.get("codeBudget")),getValue(dataList.get("codeService")),getValue(dataList.get("nomTitulaire")) 	
//				  ,getValue(dataList.get("numeroMarche")) ,getValue(dataList.get("nomFournisseur")),getValue(dataList.get("codeServiceEmetteur")) };  
//    	  
//    	  Csvwriter.writeNext(header);
//    	  
//       Csvwriter.writeNext(info);   
//    	
//       Csvwriter.close();
//      
//       addFileContent(nodeCsvFile, file);
//       
//       
//       
//      
//      } else {
//    	  
//    	  String[] info = {document.getNomDocument() ,document.getNomSite(), document.getPathDoc(), document.getNodeRef().toString(),dateTraitement
//    			  ,String.valueOf(erreur_metadonnees),String.valueOf(erreur_grave),document.getMetadonnesManquante()
//    			  ,getValue(dataList.get("exercice")) ,getValue(dataList.get("dateEmissionY")),getValue(dataList.get("typeDocument"))
//				  ,getValue(dataList.get("codeBudget")),getValue(dataList.get("codeService")),getValue(dataList.get("nomTitulaire")) 	
//				  ,getValue(dataList.get("numeroMarche")) ,getValue(dataList.get("nomFournisseur")),getValue(dataList.get("codeServiceEmetteur")) };  
//    	  
//         Csvwriter.writeNext(info);   
//        	      
//         Csvwriter.close();
//        	      
//         addFileContent(nodeCsvFile, file);
//          
//      }
      
      
      if(reader == null) {
    	 logger.debug("reader is null");
    	  String[] header = {"Nom Document" , "Nom Site" , "Chemin" , "Node Ref" , "date erreur" ,"erreur metadonnees" , "erreur grave" ,
          "metadonnee manquante" , "exercice" , "dateEmissionY" , "typeDocument" , "codeBudget" , 
           "codeService" , "nomTitulaire" , "numeroMarche" , "nomFournisseur", "codeServiceEmetteur"};
    	  
    	  listData.add(header);
    	  
      }
      
    
//    	  
//      String[] info = {document.getNomDocument() ,document.getNomSite(), document.getPathDoc(), document.getNodeRef().toString(),dateTraitement
//			  ,String.valueOf(erreur_metadonnees),String.valueOf(erreur_grave),document.getMetadonnesManquante()
//			  ,getValue(dataList.get("exercice")) ,getValue(dataList.get("dateEmissionY")),getValue(dataList.get("typeDocument"))
//			  ,getValue(dataList.get("codeBudget")),getValue(dataList.get("codeService")),getValue(dataList.get("nomTitulaire")) 	
//			  ,getValue(dataList.get("numeroMarche")) ,getValue(dataList.get("nomFournisseur")),getValue(dataList.get("codeServiceEmetteur")) };  
      
      
      logger.debug(document.getNomDocument()+" "+document.getNomSite()+" "+document.getPathDoc()+ " "+document.getNodeRef().toString());
      
      
      String[] info = {document.getNomDocument() ,document.getNomSite(), document.getPathDoc(), document.getNodeRef().toString(),dateTraitement
			  ,(String.valueOf(erreur_metadonnees) != null ) ? String.valueOf(erreur_metadonnees) : "" 
			  ,(String.valueOf(erreur_grave)!= null ? String.valueOf(erreur_grave) : "")
			  ,(document.getMetadonnesManquante()!= null) ? document.getMetadonnesManquante() : ""
			  ,(getValue(dataList.get("exercice"))!= null) ? getValue(dataList.get("exercice")) : "" 
			  ,(getValue(dataList.get("dateEmissionY")) != null) ? getValue(dataList.get("dateEmissionY")) : "" 
			  ,(getValue(dataList.get("typeDocument"))!= null) ? getValue(dataList.get("typeDocument")) : ""
			  ,(getValue(dataList.get("codeBudget")) != null) ? getValue(dataList.get("codeBudget")) :"" 
			  ,(getValue(dataList.get("codeService")) != null) ? getValue(dataList.get("codeService")) : "" 
			  ,(getValue(dataList.get("nomTitulaire"))	!= null) ? getValue(dataList.get("nomTitulaire")) : ""
			  ,(getValue(dataList.get("numeroMarche")) != null) ?  getValue(dataList.get("numeroMarche")) : ""
			  ,(getValue(dataList.get("nomFournisseur")) != null) ? getValue(dataList.get("nomFournisseur")) : "" 
			  ,(getValue(dataList.get("codeServiceEmetteur"))!= null) ? getValue(dataList.get("codeServiceEmetteur")) :"" };  
      
	  
      listData.add(info);
      
      logger.debug("list data size"+ listData.size());
   
      List<String> collect = listData.stream()
              .map(this::convertToCsvFormat)
              .collect(Collectors.toList());
      
      logger.debug("collect size"+ collect.size());
      
      InputStream inputStream = new ByteArrayInputStream(String.join(System.lineSeparator(), collect).getBytes(StandardCharsets.UTF_8));
       
      logger.debug("inputStream"+ inputStream);
      
      
      writer = serviceRegistry.getContentService().getWriter(nodeCsvFile, ContentModel.PROP_CONTENT, true);
      
      logger.debug("writer"+ inputStream);
      
      if(reader != null && reader.getSize()>0) {
			
		   InputStream inputStreamAncien = reader.getContentInputStream();
		   
		   logger.debug("inputStreamAncien"+ inputStreamAncien);
		   
		   
		   InputStream totalInputStream = new SequenceInputStream(
				   inputStreamAncien,
				    new SequenceInputStream(
				        new ByteArrayInputStream("\n".getBytes()), // gives an endline between the provided files
				        inputStream));
		   
		   logger.debug("totalInputStream"+ totalInputStream);
		   
		   writer.putContent(totalInputStream);
		    
		  
		    
		}else {
			  logger.debug("inputStream"+ inputStream);
			 writer.putContent(inputStream);  
		}
		
      
      //déplacer le document sous le répertoire Documents financiers non classés
      
      deplacerDocumentErrone(document);
      
      return true;
     
    } catch (Exception e) {
	       
    	logger.error("erreur lors de la creation du fichier csv des documents non classes: "+ e);
    	return false;
	}

}       
	     

  
  public void addFileContent(NodeRef nodeRef, File fileContent) {
		 
      boolean updateContentPropertyAutomatically = true;
      
      ContentWriter writer = getServiceRegistry().getContentService().getWriter(nodeRef, ContentModel.PROP_CONTENT, updateContentPropertyAutomatically);
      
      writer.setMimetype(MimetypeMap.MIMETYPE_TEXT_CSV);
    
      writer.putContent(fileContent);
     
  }
  
  
  public int getNbDocNonClasses(String csvName) {
		
		
		int nbDocNomClasses = 0;
		
		NodeRef nodeParentCSV = recupererNodeParentcsv(null);
		
		NodeRef nodeCSV = nodeService.getChildByName(nodeParentCSV, ContentModel.ASSOC_CONTAINS, csvName);
		
		ContentReader reader = serviceRegistry.getContentService().getReader(nodeCSV, ContentModel.PROP_CONTENT);
	     
		 String [] listErreurCSVLines = reader.getContentString().split("\n");
	       
	          for (int i = 0; i < listErreurCSVLines.length; i++) {
	        	  
	        	  nbDocNomClasses++;
	          }
		
		return nbDocNomClasses;
	}


	public void deplacerDocumentErrone(Document document) {
		
	NodeRef folderDocNonClasses = serviceCommon.getChildNodeRef(serviceCommon.getCompanyHome(), Constantes.DOCUMENTS_FINANCIERS_NON_CLASSES);
	

	// si le dossier des documents non classés n'existe pas alors on le crée
	if(folderDocNonClasses==null) {
		
		folderDocNonClasses = serviceCommon.createFolder(serviceCommon.getCompanyHome(), Constantes.DOCUMENTS_FINANCIERS_NON_CLASSES);
		
	}
	
	 
	String safeName = serviceCommon.getSafeName(folderDocNonClasses, document.getNomDocument());
	
		
	 try {
			fileFolderService.move(document.getNodeRef(), folderDocNonClasses, safeName);
		 } catch (FileExistsException | FileNotFoundException e) {

			logger.debug("erreur lors de déplacement du document "+ document.getNomDocument()+" sous le dossier "+ Constantes.DOCUMENTS_FINANCIERS_NON_CLASSES+ e);

		}

  }
	
	public Map<String, String>  getMetadonneesDocument(NodeRef nodeRefDoc, List<String>listeMetadonnes) {
		
		Map<String, String> metadataList = new HashMap<String, String>();
		
		String propertyValue = null;
	    String propertyName = null;
		 
		for(String metadonnee :listeMetadonnes) {
			
			 logger.debug("metadonnee"+ metadonnee);
	    	  
	    	  String metadonneeValue = metadonnee.substring(1, metadonnee.length() - 1);
	    	  propertyValue = (String) nodeService.getProperty(nodeRefDoc,serviceCommon.getQnameByMetadata(metadonneeValue));
	    	  propertyName = metadonnee.substring(metadonnee.indexOf(":") + 1, metadonnee.length()-1);
       
	    	 logger.debug("propertyName: "+propertyName+"-----"+"propertyValue: "+ propertyValue);
	    	
	    	 metadataList.put(propertyName, propertyValue);
	      }
		
		return metadataList;
	}
	
	
	public NodeRef recupererNodeParentcsv(Document doc) {
		
		Map<String, String> infoEnvoiMail = serviceCommon.lectureFichierConfigurationEnvoiMail();
	      
	    String cheminCsv = infoEnvoiMail.get("cheminDestinationCSV");
	    
	    if(doc != null) {
	    	doc.setPathDoc(cheminCsv);
	    }
	      
	    String [] tabCheminParentCsv = cheminCsv.split("/");
	      
	    int i = 0;
	      
	    NodeRef nodeRefRacine  = serviceCommon.getCompanyHome();
	     
	      while(i < tabCheminParentCsv.length-1) {
	    	  
	    	  NodeRef	 nodeRefParentCSV = serviceCommon.getChildNodeRef(nodeRefRacine, tabCheminParentCsv[i+1]);
	    	   
	    	  nodeRefRacine = nodeRefParentCSV;
	    	  
	    	  i++;
	      }
	      
		return nodeRefRacine;
	}
	
   
	private String getValue(String value) {
		
		String result = null;
		
		if(value == null) {
		  	  
			result = "";
		  	  
		}else if(StringUtils.isEmpty(value)) {
			result = "manquante";
		
		}else {
			result =  value;
		}
		  
		return result;  
	}
	
	public String formatCsvField(final String field, final boolean quote) {

        String result = field;

        if (result.contains(Constantes.COMMA)
                || result.contains(Constantes.DOUBLE_QUOTES)
                || result.contains(Constantes.NEW_LINE_UNIX)
                || result.contains(Constantes.NEW_LINE_WINDOWS)) {

            // if field contains double quotes, replace it with two double quotes \"\"
            result = result.replace(Constantes.DOUBLE_QUOTES, Constantes.EMBEDDED_DOUBLE_QUOTES);

            // must wrap by or enclosed with double quotes
            result = Constantes.DOUBLE_QUOTES + result + Constantes.DOUBLE_QUOTES;

        } else {
            // should all fields enclosed in double quotes
            if (quote) {
                result = Constantes.DOUBLE_QUOTES + result + Constantes.DOUBLE_QUOTES;
            }
        }

         
        return result;

    }
	
	
	  public String convertToCsvFormat(final String[] line, final String separator) {
          return convertToCsvFormat(line, separator, true);
     }
	
	 public String convertToCsvFormat(final String[] line) {
	        return convertToCsvFormat(line, Constantes.DEFAULT_SEPARATOR);
	    }


	 
	 public String convertToCsvFormat(
	         final String[] line,
	         final String separator,
	         final boolean quote) {
	 
	     return Stream.of(line)                              // convert String[] to stream
	             .map(l -> formatCsvField(l, quote))         // format CSV field
	             .collect(Collectors.joining(separator));    // join with a separator
	
	 }
    
	/**
	 * @return the serviceCommon
	 */
	public ServiceCommon getServiceCommon() {
		return serviceCommon;
	}


	/**
	 * @param serviceCommon the serviceCommon to set
	 */
	public void setServiceCommon(ServiceCommon serviceCommon) {
		this.serviceCommon = serviceCommon;
	}

	/**
	 * @return the serviceRegistry
	 */
	public ServiceRegistry getServiceRegistry() {
		return serviceRegistry;
	}

	/**
	 * @param serviceRegistry the serviceRegistry to set
	 */
	public void setServiceRegistry(ServiceRegistry serviceRegistry) {
		this.serviceRegistry = serviceRegistry;
	}
	
	
	
	

}
