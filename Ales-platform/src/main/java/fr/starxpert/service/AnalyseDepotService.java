package fr.starxpert.service;

import java.util.ArrayList;
import java.util.List;

import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.QueryConsistency;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import fr.starxpert.model.AlesDocumentModel;

public class AnalyseDepotService {

	private static Log logger = LogFactory.getLog(AnalyseDepotService.class);

	private ServiceRegistry serviceRegistry;
	
	
	/**
	 * Create search parameter with TRANSACTIONAL QueryConsistency and fts-alfresco
	 * language
	 *
	 * @param query
	 * @return SearchParameters
	 */
	private SearchParameters getSearchParameters(String query) {

		SearchParameters searchParameters = new SearchParameters();

		searchParameters.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
		searchParameters.setQueryConsistency(QueryConsistency.TRANSACTIONAL_IF_POSSIBLE);
		searchParameters.setLanguage(SearchService.LANGUAGE_FTS_ALFRESCO);
		searchParameters.setQuery(query);

		return searchParameters;
	}
	
	
	
	/**
	 * recuperation des repertoires SAS
	 *
	 * @return liste de nodeRef des documents qui sont dans le SAS
	 */
	public List<NodeRef> getSasAlesFolders() {

		List<NodeRef> foldersSasALS = new ArrayList<>();

		String queryAspectSASALS = "=EXACTASPECT:'" + AlesDocumentModel.QNAME_ASPECT_DOSSIER_SAS_ENTREE + "'";

		// On lance la requete pour recuperer tous les repertoires possedant l'aspect
		// dvo:sasDepotCsv
		ResultSet resultSet = serviceRegistry.getSearchService().query(getSearchParameters(queryAspectSASALS));

		logger.debug("NOMBRE DE FOLDER SAS TROUVE : " + resultSet.length());

		// Pour chaque resultat on recupere le nodeRef et on l'introduit dans
		// foldersSasGDA
		if (resultSet.length() != 0) {

			for (int i = 0; i < resultSet.length(); i++) {
				foldersSasALS.add(resultSet.getNodeRef(i));
			}
		}

		return foldersSasALS;
	}



	/**
	 * @param serviceRegistry the serviceRegistry to set
	 */
	public void setServiceRegistry(ServiceRegistry serviceRegistry) {
		this.serviceRegistry = serviceRegistry;
	}
	
	
	
	
}
